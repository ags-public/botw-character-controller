using UnityEngine;
using UnityEngine.InputSystem;
using AGS.Player.Input;
using AGS.Player.FSM;

namespace AGS.Player
{
    [System.Serializable]
    public class PlayerStateInput : PlayerComponentBase
    {
        PlayerInputActions.PlayerActions m_PlayerActions;

        [SerializeField] Vector2 m_Move;
        [SerializeField] Vector2 m_Look;

        [Header("Movement Settings")]
		public bool analogMovement;

		[Header("Mouse Cursor Settings")]
		public bool cursorLocked = true;
		public bool cursorInputForLook = true;

        internal override void InitializeComponent(GameObject playerObject)
        {
            base.InitializeComponent(playerObject);
            m_PlayerActions = new PlayerInputActions().Player;
            m_PlayerActions.Enable();

            m_PlayerActions.Jump.performed += OnJump;
            m_PlayerActions.Jump.canceled += OnJump;

            m_PlayerActions.Sprint.performed += OnSprint;
            m_PlayerActions.Sprint.canceled += OnSprint;

            m_PlayerActions.Attack1.performed += OnAttack1;
            m_PlayerActions.Attack1.canceled += OnAttack1;

            m_PlayerActions.Attack2.performed += OnAttack2;
            m_PlayerActions.Attack2.canceled += OnAttack2;

            m_PlayerActions.Weapon1.performed += OnWeapon1;
            m_PlayerActions.Weapon1.canceled += OnWeapon1;

            m_PlayerActions.Weapon2.performed += OnWeapon2;
            m_PlayerActions.Weapon2.canceled += OnWeapon2;

            m_PlayerActions.Weapon3.performed += OnWeapon3;
            m_PlayerActions.Weapon3.canceled += OnWeapon3;
        }

        internal void UpdateInput()
        {
            m_Move = m_PlayerActions.Move.ReadValue<Vector2>();

            if (cursorInputForLook)
                m_Look = m_PlayerActions.Look.ReadValue<Vector2>();
        }

		void OnJump(InputAction.CallbackContext context)
		{
            Jump = context.ReadValue<float>() > 0.5f;
		}

		void OnSprint(InputAction.CallbackContext context)
		{
            Sprint = context.ReadValue<float>() > 0.5f;
		}

        void OnAttack1(InputAction.CallbackContext context)
        {
            Attack1 = context.ReadValue<float>() > 0.5f;
        }

        void OnAttack2(InputAction.CallbackContext context)
        {
            Attack2 = context.ReadValue<float>() > 0.5f;
        }

        void OnWeapon1(InputAction.CallbackContext context)
        {
            Weapon1 = context.ReadValue<float>() > 0.5f;
        }

        void OnWeapon2(InputAction.CallbackContext context)
        {
            Weapon2 = context.ReadValue<float>() > 0.5f;
        }

        void OnWeapon3(InputAction.CallbackContext context)
        {
            Weapon3 = context.ReadValue<float>() > 0.5f;
        }

        void OnApplicationFocus(bool hasFocus)
		{
			SetCursorState(cursorLocked);
		}

		void SetCursorState(bool newState)
		{
			Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
		}

		public Vector2 Move => m_Move;
        public Vector2 Look => m_Look;
        public bool Jump { private set; get; }
        public bool Sprint { private set; get; }
        public bool Attack1 { private set; get; }
        public bool Attack2 { private set; get; }

        public bool Weapon1 { private set; get; }
        public bool Weapon2 { private set; get; }
        public bool Weapon3 { private set; get; }

        public float InputMagnitude => analogMovement ? Move.magnitude : 1f;
    }
	
}