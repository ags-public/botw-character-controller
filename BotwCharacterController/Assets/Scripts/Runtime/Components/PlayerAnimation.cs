using AGS.Player.FSM;
using System.Collections;
using UnityEngine;

namespace AGS.Player
{
    [System.Serializable]
    public class PlayerAnimation : PlayerComponentBase
    {
        public const int k_CombatLayer = 1;
        // animation IDs
        public static int k_AnimIDSpeed;
        public static int k_AnimIDMotionSpeed;

        public static int k_AnimIDGrounded;
        public static int k_AnimIDJump;
        public static int k_AnimIDFreeFall;
        public static int k_AnimIDClimb;

        public static int k_ReleaseAttack;
        public static int k_ChargeAttack;
        public static int k_WeaponSlot;

        [SerializeField] Animator m_Animator;
        Coroutine m_AnimationCoroutine;

        AnimatorEvents m_AnimatorEvents;
        internal AnimatorEvents Events => m_AnimatorEvents;

        public void AddEvent(string name, System.Action callback)
        {

        }

        internal override void InitializeComponent(GameObject playerObject)
        {
            base.InitializeComponent(playerObject);
            AssignAnimationIDs();

            m_AnimatorEvents = playerObject.GetComponent<AnimatorEvents>();
        }

        void AssignAnimationIDs()
        {
            k_AnimIDSpeed = Animator.StringToHash("Speed");
            k_AnimIDMotionSpeed = Animator.StringToHash("MotionSpeed");

            k_AnimIDGrounded = Animator.StringToHash("Grounded");
            k_AnimIDJump = Animator.StringToHash("Jump");
            k_AnimIDClimb = Animator.StringToHash("Climb");
            k_AnimIDFreeFall = Animator.StringToHash("FreeFall");

            k_ReleaseAttack = Animator.StringToHash("ReleaseAttack");
            k_ChargeAttack = Animator.StringToHash("ChargeAttack");

            k_WeaponSlot = Animator.StringToHash("WeaponSlot");
        }

        public int PlayAnimation(string stateName, System.Action onComplete, int layer)
        {
            if(m_AnimationCoroutine == null)
            {
                if (false == string.IsNullOrEmpty(stateName))
                    m_Animator.Play(stateName, layer);
                //fix
                //m_AnimationCoroutine = StartCoroutine(Coroutine_OnAnimationComplete(layer, onComplete));

                return m_Animator.GetCurrentAnimatorStateInfo(layer).shortNameHash;
            }

            Debug.LogWarning("PlayAnimation >> there is an active coroutine");
            return -1;
        }

        IEnumerator Coroutine_OnAnimationComplete(int layer, System.Action onComplete)
        { 
            var stateInfo = m_Animator.GetCurrentAnimatorStateInfo(layer);
            var startState = stateInfo.shortNameHash;

            do
            {
                yield return null;
                stateInfo = m_Animator.GetCurrentAnimatorStateInfo(layer);

            } while (stateInfo.shortNameHash == startState);

            onComplete?.Invoke();
            m_AnimationCoroutine = null;
        }

        public void SetTrigger(int id) => m_Animator?.SetTrigger(id);

        public void SetInt(int id, int value) => m_Animator?.SetInteger(id, value);

        public void SetBool(int id, bool value) => m_Animator?.SetBool(id, value);

        public void SetFloat(int id, float value) => m_Animator?.SetFloat(id, value);

        public float GetFloat(int id) => m_Animator?.GetFloat(id) ?? 0f;

        internal void ToggleLayerWeight(int layer, bool enable)
        {
            m_Animator.SetLayerWeight(layer, enable ? 1f : 0f);
        }

        internal void MoveAnimation(float targetSpeed, float speedChangeRate, float inputMagnitude)
        {
            //animation
            float _animationBlend = GetFloat(k_AnimIDSpeed);
            _animationBlend = Mathf.Lerp(_animationBlend, targetSpeed, Time.deltaTime * speedChangeRate);
            if (_animationBlend < 0.01f) _animationBlend = 0f;

            SetFloat(k_AnimIDSpeed, _animationBlend);
            SetFloat(k_AnimIDMotionSpeed, inputMagnitude);
        }

        private void OnFootstep(AnimationEvent animationEvent)
        {
            //if (animationEvent.animatorClipInfo.weight > 0.5f)
            //{
            //    if (FootstepAudioClips.Length > 0)
            //    {
            //        var index = Random.Range(0, FootstepAudioClips.Length);
            //        AudioSource.PlayClipAtPoint(FootstepAudioClips[index], transform.TransformPoint(_controller.center), FootstepAudioVolume);
            //    }
            //}
        }

        private void OnLand(AnimationEvent animationEvent)
        {
            //if (animationEvent.animatorClipInfo.weight > 0.5f)
            //{
            //    AudioSource.PlayClipAtPoint(LandingAudioClip, transform.TransformPoint(_controller.center), FootstepAudioVolume);
            //}
        }
    }
}


