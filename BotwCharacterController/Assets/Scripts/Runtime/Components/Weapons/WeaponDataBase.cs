using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGS.Player.Weapons
{
    public enum WeaponSlot
    {
        Punch,
        Sword,
        Bow
    }

    public enum WeaponType
    {
        Melee,
        Ranged
    }

    public abstract class WeaponDataBase : ScriptableObject
    {
        public string weaponName;
        public WeaponType weaponType;
    }
}
