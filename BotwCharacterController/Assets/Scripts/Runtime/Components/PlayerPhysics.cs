using AGS.Player.FSM;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGS.Player
{
    [System.Serializable]
    public class PlayerPhysics : PlayerComponentBase
    {
        static readonly Color transparentGreen = new Color(0.0f, 1.0f, 0.0f, 0.35f);
        static readonly Color transparentRed = new Color(1.0f, 0.0f, 0.0f, 0.35f);

        [Tooltip("The character uses its own gravity value. The engine default is -9.81f")]
        bool m_ApplyGravity = true;
        [SerializeField] float m_Gravity = -15.0f;

        [Header("Grounded")]
        [SerializeField] bool m_Grounded = true;

        [Tooltip("Useful for rough ground")]
        public float GroundedOffset = -0.14f;

        [Tooltip("The radius of the grounded check. Should match the radius of the CharacterController")]
        public float GroundedRadius = 0.28f;

        public LayerMask GroundLayers;

        [Header("Climb")]
        [SerializeField] bool m_Climbable = true;
        public LayerMask ClimbLayers;
        public float ClimbRadius = 0.1f;

        CharacterController m_CharacterController;
        Vector3 m_Velocity;

        float _terminalVelocity = 53.0f;

        Vector3 m_GroundSpherePosition;
        Vector3 m_ClimbSpherePosition;

        //
        float m_Speed;
        Vector3 m_TargetDirection;
        float m_TargetRotation = 0.0f;
        float m_RotationVelocity = 0f;

        internal override void InitializeComponent(GameObject playerObject)
        {
            base.InitializeComponent(playerObject);
            m_CharacterController = playerObject.GetComponent<CharacterController>();
        }

        internal bool CheckGrounded()
        {
            m_GroundSpherePosition = transform.position;
            m_GroundSpherePosition.y = transform.position.y - GroundedOffset;

            m_Grounded = Physics.CheckSphere(m_GroundSpherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);
            return m_Grounded;
        }

        internal bool CheckClimbable(float forwardOffset, float upwardsOffest, out Vector3 checkPosition)
        {
            checkPosition = transform.position + (transform.forward * forwardOffset);
            checkPosition.y += upwardsOffest;
            m_ClimbSpherePosition = checkPosition;

            m_Climbable = Physics.CheckSphere(checkPosition, GroundedRadius, ClimbLayers, QueryTriggerInteraction.Ignore);
            return m_Climbable;
        }

        internal Vector3 FindClimbExitPoint(Vector3 checkPosition)
        {
            if( Physics.Raycast(checkPosition, Vector3.down, out RaycastHit hit, 1.8f, GroundLayers) )
                return hit.point;

            return checkPosition;
        }

        public void UpdatePhysics()
        {
            ApplyGravity();

            // move the player
            m_CharacterController.Move(m_Velocity * Time.deltaTime);
        }

        void ApplyGravity()
        {
            if (!m_ApplyGravity) return;

            // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
            if (!m_Grounded && m_Velocity.y < _terminalVelocity)
            {
                m_Velocity.y += m_Gravity * Time.deltaTime;
            }
        }

        internal void Move(PlayerStateInput Input, float rotationY, float targetSpeed, float speedChangeRate, float rotationSmoothTime)
        {
            // a reference to the players current horizontal velocity
            float currentHorizontalSpeed = new Vector3(ControllerVelocity.x, 0.0f, ControllerVelocity.z).magnitude;

            float speedOffset = 0.1f;
            float inputMagnitude = Input.analogMovement ? Input.Move.magnitude : 1f;

            // accelerate or decelerate to target speed
            if (currentHorizontalSpeed < targetSpeed - speedOffset ||
                currentHorizontalSpeed > targetSpeed + speedOffset)
            {
                // creates curved result rather than a linear one giving a more organic speed change
                // note T in Lerp is clamped, so we don't need to clamp our speed
                m_Speed = Mathf.Lerp(currentHorizontalSpeed, targetSpeed * inputMagnitude,
                    Time.deltaTime * speedChangeRate);

                // round speed to 3 decimal places
                m_Speed = Mathf.Round(m_Speed * 1000f) / 1000f;
            }
            else
            {
                m_Speed = targetSpeed;
            }

            // normalise input direction
            Vector3 inputDirection = new Vector3(Input.Move.x, 0.0f, Input.Move.y).normalized;

            // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
            // if there is a move input rotate player when the player is moving
            if (Input.Move != Vector2.zero)
            {
                m_TargetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + rotationY;
                float rotation = Mathf.SmoothDampAngle(transform.eulerAngles.y, m_TargetRotation, ref m_RotationVelocity, rotationSmoothTime);

                // rotate to face input direction relative to camera position
                transform.rotation = Quaternion.Euler(0.0f, rotation, 0.0f);
            }

            m_TargetDirection = Quaternion.Euler(0.0f, m_TargetRotation, 0.0f) * Vector3.forward;

            var horizontalVelocity = m_TargetDirection.normalized * m_Speed;
            var verticalVelocity = new Vector3(0, TargetVelocity.y, 0);

            SetVelocity(horizontalVelocity + verticalVelocity);
        }

        void OnDrawGizmosSelected()
        {
            Gizmos.color = m_Grounded ? transparentGreen : transparentRed;
            Gizmos.DrawSphere(m_GroundSpherePosition, GroundedRadius);

            Gizmos.color = m_Climbable ? transparentGreen : transparentRed;
            Gizmos.DrawSphere(m_ClimbSpherePosition, ClimbRadius);
        }

        public void SetVelocity(Vector3 newVelocity) => m_Velocity = newVelocity;

        public void EnableGravity(bool enable) => m_ApplyGravity = enable;

        public Vector3 HorizontalVelocity => m_TargetDirection.normalized * m_Speed;
        public Vector3 ControllerVelocity => m_CharacterController.velocity;
        public Vector3 TargetVelocity => m_Velocity;

        public float Gravity => m_Gravity;

        public bool Grounded => m_Grounded;
    }
}


