using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AGS.Player.Weapons;
using AGS.Player.FSM;

namespace AGS.Player
{
    [System.Serializable]
    public class WeaponObject
    {
        public WeaponDataBase weaponData;
        public GameObject activeWeapon;
        public GameObject storedWeapon;

        internal void ToggleWeapon(bool hold)
        {
            activeWeapon.SetActive(hold);
            if (storedWeapon)
                storedWeapon.SetActive(!hold);
        }
    }

    [System.Serializable]
    //A class that enables and disabled the weapons of the player
    public class PlayerWeapons : PlayerComponentBase
    {
        [SerializeField] WeaponObject[] weapons;

        WeaponObject m_CurrentWeapon;
        WeaponSlot m_CurrentWeaponSlot;

        internal override void InitializeComponent(GameObject playerObject)
        {
            base.InitializeComponent(playerObject);
            foreach (var weapon in weapons)
            {
                weapon.ToggleWeapon(false);
            }

            EquipWeapon(0);
        }

        internal void EquipWeapon(WeaponSlot slot)
        {
            m_CurrentWeaponSlot = slot;

            //deactivate previous weapon
            if (null != m_CurrentWeapon)
            {
                m_CurrentWeapon.ToggleWeapon(false);
            }

            //activate new weapon
            m_CurrentWeapon = weapons[(int)slot];
            m_CurrentWeapon.ToggleWeapon(true);
        }

        public WeaponSlot CurrentWeaponSlot => m_CurrentWeaponSlot;
    }
}