using AGS.Player.FSM;
using UnityEngine;

namespace AGS.Player
{
    [System.Serializable]
    public class PlayerCamera : PlayerComponentBase
    {
        const float k_Threshold = 0.01f;

        GameObject m_MainCamera;
        // cinemachine
        float _cinemachineTargetYaw;
        float _cinemachineTargetPitch;

        [Header("Default")]
        [Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
        public Transform DefaultTarget;

        [Tooltip("How far in degrees can you move the camera up")]
        public float TopClamp = 70.0f;

        [Tooltip("How far in degrees can you move the camera down")]
        public float BottomClamp = -30.0f;

        [Tooltip("Additional degress to override the camera. Useful for fine tuning camera position when locked")]
        public float CameraAngleOverride = 0.0f;

        [Tooltip("For locking the camera position on all axis")]
        public bool LockCameraPosition = false;

        [Header("Bow")]
        public GameObject bowCamera;

        internal override void InitializeComponent(GameObject playerObject)
        {
            base.InitializeComponent(playerObject);
            _cinemachineTargetYaw = DefaultTarget.rotation.eulerAngles.y;

            // get a reference to our main camera
            if (m_MainCamera == null)
                m_MainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        }

        internal void CameraRotation(PlayerStateInput _input)
        {
            // if there is an input and camera position is not fixed
            if (_input.Look.sqrMagnitude >= k_Threshold && !LockCameraPosition)
            {
                //Don't multiply mouse input by Time.deltaTime;
                float deltaTimeMultiplier = 1f;// IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;

                _cinemachineTargetYaw += _input.Look.x * deltaTimeMultiplier;
                _cinemachineTargetPitch += _input.Look.y * deltaTimeMultiplier;
            }

            // clamp our rotations so our values are limited 360 degrees
            _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
            _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

            // Cinemachine will follow this target
            DefaultTarget.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
                _cinemachineTargetYaw, 0.0f);
        }

        static float ClampAngle(float lfAngle, float lfMin, float lfMax)
        {
            if (lfAngle < -360f) lfAngle += 360f;
            if (lfAngle > 360f) lfAngle -= 360f;
            return Mathf.Clamp(lfAngle, lfMin, lfMax);
        }

        internal void ToggleAimCamera(bool enable)
        { 
            bowCamera.SetActive(enable);
        }

        public GameObject MainCamera => m_MainCamera;

        //        bool IsCurrentDeviceMouse
        //        {
        //            get
        //            {
        //#if ENABLE_INPUT_SYSTEM
        //                return _playerInput.currentControlScheme == "KeyboardMouse";
        //#else
        //				return false;
        //#endif
        //            }
        //        }
    }

}