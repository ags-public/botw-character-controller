using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGS.Player.FSM
{
    public class MeleeQuickAttackState : PlayerStateBase
    {
        public override void Init(PlayerStateSystem playerSystem)
        {
            base.Init(playerSystem);
            //Animator.AddEvent();
            Animator.Events.onAttackEnds += ExitState;
        }

        public override void OnEnter(object[] _data)
        {
            int slot = (int)Weapons.CurrentWeaponSlot;
            string state = $"Quick Attack {slot}";

            Animator.PlayAnimation(state, null, PlayerAnimation.k_CombatLayer);
        }

        public override void OnExit() { }

        void ExitState()
        {
            //animation end
            NormalState.PerformTransition(this);
        }

        internal static void PerformTransition(PlayerStateBase state)
        {
            state.StateSystem.PerformTransition(nameof(MeleeQuickAttackState));
        }
    }
}