using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.XR;

namespace AGS.Player.FSM
{
    public class AirborneState : PlayerStateBase
    {
        [Space(10)]
        [Tooltip("The height the player can jump")]
        public float JumpHeight = 1.2f;

        [Space(10)]
        [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
        public float JumpTimeout = 0.50f;

        [Tooltip("Time required to pass before entering the fall state. Useful for walking down stairs")]
        public float FallTimeout = 0.15f;

        float _fallTimeoutDelta;

        [Header("Climb")]
        public float ClimbOffsetUpward = 0.93f;
        public float ClimbOffsetForward = 0.2f;

        public override void OnEnter(object[] _data)
        {
            // reset the fall timeout timer
            _fallTimeoutDelta = FallTimeout;

            bool jump = (bool)_data[0];

            if(jump)
            {
                var velocity = (Vector3)_data[1];
                var jumpScale = (float)_data[2];
                // the square root of H * -2 * G = how much velocity needed to reach desired height
                velocity.y = Mathf.Sqrt(JumpHeight * jumpScale * -2f * Physics.Gravity);
                Physics.SetVelocity(velocity);

                Animator.SetBool(PlayerAnimation.k_AnimIDJump, true);
            }

            Animator.SetBool(PlayerAnimation.k_AnimIDGrounded, false);
        }

        public override void OnExit()
        {
            Animator.SetBool(PlayerAnimation.k_AnimIDJump, false);
            Animator.SetBool(PlayerAnimation.k_AnimIDFreeFall, false);
        }

        public override void Update()
        {
            // fall timeout
            if (_fallTimeoutDelta >= 0.0f)
            {
                _fallTimeoutDelta -= Time.deltaTime;
            }
            else
            {
                Animator.SetBool(PlayerAnimation.k_AnimIDFreeFall, true);

                bool grounded = Physics.CheckGrounded();
                if (grounded)
                {
                    NormalState.PerformTransition(this);
                    return;
                }
                else
                {
                    //if(Input.Jump)
                    //{
                    //    if (PerformTransition(nameof(GlideState)))
                    //        return;
                    //}
                }
            }

            this.Physics.UpdatePhysics();
        }

        public override void FixedUpdate()
        {
            CheckClimb();
        }

        void CheckClimb()
        {
            Vector3 checkPosition;
            if (Input.Move.y > 0.1f && Physics.CheckClimbable(ClimbOffsetForward, ClimbOffsetUpward, out checkPosition))
            {
                ClimbState.PerformTransition(this);
            }
        }

        internal static void PerformTransition(PlayerStateBase state, bool jump, Vector3 velocity, float jumpScale = 1f)
        {
            state.StateSystem.PerformTransition(nameof(AirborneState), jump, velocity, jumpScale);
        }
    }
}