﻿using UnityEngine;
using AGS.Player.Weapons;

namespace AGS.Player.FSM
{
    public class NormalState : PlayerStateBase
    {
        [Tooltip("Move speed of the character in m/s")]
        public float MoveSpeed = 2.0f;

        [Tooltip("Sprint speed of the character in m/s")]
        public float SprintSpeed = 5.335f;

        [Tooltip("How fast the character turns to face movement direction")]
        [Range(0.0f, 0.3f)]
        public float RotationSmoothTime = 0.12f;

        [Tooltip("Acceleration and deceleration")]
        public float SpeedChangeRate = 10.0f;

        [Space(10)]
        [Tooltip("Time required to pass before being able to jump again. Set to 0f to instantly jump again")]
        public float JumpTimeout = 0.50f;
        // timeout deltatime
        float _jumpTimeoutDelta;

        public override void Init(PlayerStateSystem playerSystem)
        {
            base.Init( playerSystem );

            // reset our timeouts on start
            _jumpTimeoutDelta = JumpTimeout;
        }

        public override void OnEnter( object[] _data )
        {
            // reset the jump timeout timer
            _jumpTimeoutDelta = JumpTimeout;

            // update animator if using character
            Animator.SetBool(PlayerAnimation.k_AnimIDGrounded, true);
        }

        public override void Update()
        {
            bool grounded = Physics.CheckGrounded();

            if (grounded)
            {
                if (CheckJump()) return;

                if (CheckAttack()) return;

                if (Input.Sprint)
                    Weapons.EquipWeapon(WeaponSlot.Punch);

                float targetSpeed = (Input.Move == Vector2.zero) ? 0f : Input.Sprint ? SprintSpeed : MoveSpeed;
                float rotationY = Camera.MainCamera.transform.eulerAngles.y;

                Physics.Move(Input, rotationY, targetSpeed, SpeedChangeRate, RotationSmoothTime);
                Animator.MoveAnimation(targetSpeed, SpeedChangeRate, Input.InputMagnitude);
            }
            else
            {
                AirborneState.PerformTransition(this, false, Physics.HorizontalVelocity);
            }

            this.Physics.UpdatePhysics();
        }

        public override void LateUpdate()
        {
            Camera.CameraRotation(Input);
        }

        bool CheckAttack()
        {
            if (Input.Weapon1)
                Weapons.EquipWeapon(WeaponSlot.Punch);

            if (Input.Weapon2)
                Weapons.EquipWeapon(WeaponSlot.Sword);

            if (Input.Weapon3)
                Weapons.EquipWeapon(WeaponSlot.Bow);

            if (Input.Attack1)
            {
                MeleeQuickAttackState.PerformTransition(this);
                return true;
            }

            if (Input.Attack2)
            {
                if (Weapons.CurrentWeaponSlot == WeaponSlot.Bow)
                    RangedChargeAttackState.PerformTransition(this);
                else
                    MeleeChargeAttackState.PerformTransition(this);
                return true;
            }

            return false;
        }

        bool CheckJump()
        {
            // Jump
            if (Input.Jump && _jumpTimeoutDelta <= 0.0f)
            {
                AirborneState.PerformTransition(this, true, Physics.HorizontalVelocity, 1f);
                return true;
            }

            // jump timeout
            if (_jumpTimeoutDelta >= 0.0f)
                _jumpTimeoutDelta -= Time.deltaTime;

            return false;
        }

        internal static void PerformTransition(PlayerStateBase state)
        {
            state.StateSystem.PerformTransition(nameof(NormalState));
        }
    }
}