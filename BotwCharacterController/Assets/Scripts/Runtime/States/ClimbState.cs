using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace AGS.Player.FSM
{
    public class ClimbState : PlayerStateBase
    {
        [Tooltip("Move speed of the character in m/s")]
        public float MoveSpeed = 2.0f;
        public float ExitOffsetUpward = 0.5f;
        public float ExitOffsetForward = 0.5f;

        public float JumpDuration = 0.5f;
        public float JumpDistance = 1.75f;

        Vector3 targetDirection;
        float m_JumpTimeoutDelta;

        Tween m_JumpTween;

        public override void OnEnter(object[] _data)
        {
            Physics.EnableGravity(false);
            Animator.SetBool(PlayerAnimation.k_AnimIDClimb, true);
        }

        public override void OnExit()
        {
            Physics.EnableGravity(true);
            Animator.SetBool(PlayerAnimation.k_AnimIDClimb, false);
        }

        public override void Update()
        {
            if (m_JumpTween != null)
                return;

            ClimbJump();

            float targetSpeed = (Input.Move == Vector2.zero) ? 0.0f : MoveSpeed;

            Vector3 inputDirection = new Vector3(Input.Move.x, 0.0f, Input.Move.y).normalized;

            targetDirection = new Vector3(inputDirection.x, inputDirection.z, 0f);
            Physics.SetVelocity(targetDirection * targetSpeed);

            Physics.UpdatePhysics();

            Animator.SetFloat(PlayerAnimation.k_AnimIDSpeed, targetSpeed);
        }

        public override void FixedUpdate()
        {
            if (m_JumpTween != null)
                return;

            if(Physics.CheckGrounded())
            {
                NormalState.PerformTransition(this);
            }
            else
            {
                if (!Physics.CheckClimbable(ExitOffsetForward, ExitOffsetUpward, out Vector3 checkPosition))
                {
                    ExitJump(checkPosition, 0.5f);
                }
            }
        }

        void ClimbJump()
        {
            if (Input.Jump && m_JumpTween == null)
            {
                if (Physics.CheckClimbable(ExitOffsetForward, JumpDistance, out Vector3 checkPosition))
                {
                    //climb jump
                    Vector3 offset = Vector3.up * JumpDistance;
                    m_JumpTween = transform.DOMove(transform.position + offset, JumpDuration).OnComplete(() => { m_JumpTween = null; });
                }
                else
                {
                    ExitJump(checkPosition, 0f);
                }
            }
        }

        void ExitJump(Vector3 checkPosition, float forwardOffset)
        {
            Animator.SetBool(PlayerAnimation.k_AnimIDClimb, false);

            Vector3 endPosition = Physics.FindClimbExitPoint(checkPosition);
            endPosition += transform.forward * forwardOffset;
            m_JumpTween = transform.DOMove(endPosition, JumpDuration).OnComplete(OnExitJumpComplete);
        }

        void OnExitJumpComplete()
        {
            AirborneState.PerformTransition(this, true, targetDirection.normalized, 0.1f);
            m_JumpTween = null;
        }

        internal static void PerformTransition(PlayerStateBase state)
        {
            state.StateSystem.PerformTransition(nameof(ClimbState));
        }
    }
}