using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGS.Player.FSM
{
    public class MeleeChargeAttackState : PlayerStateBase
    {
        [Tooltip("Move speed of the character in m/s")]
        public float MoveSpeed = 2.0f;
        [Tooltip("Acceleration and deceleration")]
        public float SpeedChangeRate = 10.0f;
        [Tooltip("How fast the character turns to face movement direction")]
        [Range(0.0f, 0.3f)]
        public float RotationSmoothTime = 0.12f;

        [Header("Charge Attack")]
        public float minChargeTime = 1f;
        public float maxChargeTime = 3f;
        float m_ChargeTimer;
        bool m_ReleasingAttack;

        public override void OnEnter(object[] _data)
        {
            m_ReleasingAttack = false;
            m_ChargeTimer = 0f;

            int slot = (int)Weapons.CurrentWeaponSlot;
            Animator.SetInt(PlayerAnimation.k_WeaponSlot, slot);

            Animator.SetBool(PlayerAnimation.k_ChargeAttack, true);
            Animator.SetBool(PlayerAnimation.k_ReleaseAttack, false);
        }

        public override void OnExit()
        {
            Animator.SetInt(PlayerAnimation.k_WeaponSlot, -1);
            Animator.SetBool(PlayerAnimation.k_ChargeAttack, false);
        }

        public override void Update()
        {
            if (m_ReleasingAttack) return;

            if (UpdateCharge()) return;
        }

        bool UpdateCharge()
        {
            if(Input.Attack2)
            {
                m_ChargeTimer += Time.deltaTime * ChargeTimeModifier;
            }
            else
            {
                if (m_ChargeTimer >= minChargeTime)
                {
                    m_ReleasingAttack = true;
                    m_ChargeTimer = Mathf.Clamp(m_ChargeTimer, minChargeTime, maxChargeTime);

                    Animator.SetBool(PlayerAnimation.k_ReleaseAttack, true);
                    Animator.PlayAnimation(null, ExitState, PlayerAnimation.k_CombatLayer);

                    Debug.Log($"Release Attack : {ChargeRatio}");
                }
                else
                {
                    ExitState();
                }
                return true;
            }

            return false;
        }

        void ExitState()
        {
            NormalState.PerformTransition(this);
        }

        public float ChargeRatio => (m_ChargeTimer - minChargeTime) / (maxChargeTime - minChargeTime);

        float ChargeTimeModifier => 1f;

        internal static void PerformTransition(PlayerStateBase state)
        {
            state.StateSystem.PerformTransition(nameof(MeleeChargeAttackState));
        }
    }
}