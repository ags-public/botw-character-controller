using UnityEngine;

namespace AGS.Player.FSM
{
    public class PlayerStateSystem : StateSystemBase
    {
        [Header("Components")]
        [SerializeField] PlayerStateInput inputValues;

        [SerializeField] PlayerAnimation playerAnimation;
        [SerializeField] PlayerCamera playerCamera;
        [SerializeField] PlayerPhysics playerPhysics;
        [SerializeField] PlayerWeapons playerWeapons;

        [Header("Debug")]
        [SerializeField] UIDebugPlayerState uiDebugPlayerState;

        void Start()
        {
            inputValues.InitializeComponent(gameObject);
            playerAnimation.InitializeComponent(gameObject);
            playerCamera.InitializeComponent(gameObject);
            playerPhysics.InitializeComponent(gameObject);
            playerWeapons.InitializeComponent(gameObject);

            foreach (var state in m_States)
            {
                (state as PlayerStateBase).Init(this);
            }
        }

        void Update()
        {
            inputValues.UpdateInput();
            m_CurrentState?.Update();

#if UNITY_EDITOR
            uiDebugPlayerState.AddLog(m_CurrentState.name);
            uiDebugPlayerState?.Show();
#endif
        }

        void FixedUpdate()
        {
            m_CurrentState?.FixedUpdate();
        }

        void LateUpdate()
        {
            m_CurrentState?.LateUpdate();
        }

        internal PlayerStateInput InputValues => inputValues;
        internal PlayerPhysics Physics => playerPhysics;
        internal PlayerAnimation Animation => playerAnimation;
        internal PlayerCamera Camera => playerCamera;
        internal PlayerWeapons Weapons => playerWeapons;

    }

}