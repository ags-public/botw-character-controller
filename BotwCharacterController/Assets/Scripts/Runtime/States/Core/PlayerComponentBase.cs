using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGS.Player.FSM
{
    public abstract class PlayerComponentBase
    {
        protected GameObject m_PlayerGameObject;


        internal virtual void InitializeComponent(GameObject playerObject)
        {
            m_PlayerGameObject = playerObject;
        }

        protected Transform transform => m_PlayerGameObject.transform;
    }
}