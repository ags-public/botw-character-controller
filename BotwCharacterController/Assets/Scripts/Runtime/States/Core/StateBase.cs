using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGS.Player.FSM
{

    /// <summary>
    /// This class represents the States in the Finite State System.
    /// Each state has a Dictionary with pairs (transition-state) showing
    /// which state the FSM should be if a transition is fired while this state
    /// is the current state.
    /// Method Reason is used to determine which transition should be fired .
    /// Method Act has the code to perform the actions the NPC is supposed do if it's on this state.
    /// </summary>
    public abstract class StateBase : ScriptableObject
    {
        [SerializeField] bool active = true;
        [SerializeField] StateBase[] ConnectedStates;

        Dictionary<string, StateBase> m_Map = new();

        internal bool Active => active;

        internal string TransitionKey => this.GetType().Name;

        protected void AddTransitions()
        {
            m_Map.Clear();
            foreach (var connection in ConnectedStates)
            {
                this.AddTransition(connection);
            }
        }

        internal void AddTransition(StateBase state)
        {
            // Since this is a Deterministic FSM,
            //   check if the current transition was already inside the map
            if (m_Map.ContainsKey(state.TransitionKey))
            {
                Debug.LogError($"AddTransition >> State '{state}' already has transition: {state.TransitionKey}");
                return;
            }

            m_Map.Add(state.TransitionKey, state);
        }

        /// <summary>
        /// This method deletes a pair transition-state from this state's map.
        /// If the transition was not inside the state's map, an ERROR message is printed.
        /// </summary>
        internal void DeleteTransition(string trans)
        {
            // Check if the pair is inside the map before deleting
            if (m_Map.ContainsKey(trans))
            {
                m_Map.Remove(trans);
            }
            else
            {
                Debug.LogError($"DeleteTransition >> Transition {trans} is not on the state's transition list");
            }
        }

        /// <summary>
        /// This method returns the new state the FSM should be if
        ///    this state receives a transition and 
        /// </summary>
        internal StateBase GetOutputState(string trans)
        {
            // Check if the map has this transition
            if (m_Map.ContainsKey(trans))
                return m_Map[trans];

            return null;
        }

        /// <summary>
        /// This method is used to set up the State condition before entering it.
        /// It is called automatically by the FSMSystem class before assigning it
        /// to the current state.
        /// </summary>
        public virtual void OnEnter(params object[] data) { }

        /// <summary>
        /// This method is used to make anything necessary, as reseting variables
        /// before the FSMSystem changes to another one. It is called automatically
        /// by the FSMSystem before changing to a new state.
        /// </summary>
        public virtual void OnExit() { }

        /// <summary>
        /// This method controls the behavior of the NPC in the game World.
        /// Every action, movement or communication the NPC does should be placed here
        /// NPC is a reference to the object that is controlled by this class
        /// </summary>
        public virtual void FixedUpdate() { }

        public virtual void Update() { }

        public virtual void LateUpdate() { }

    } // class FSMState


}