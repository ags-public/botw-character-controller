using UnityEngine;

namespace AGS.Player.FSM
{
    public class PlayerStateBase : StateBase
    {
        PlayerStateSystem playerSystem;

        public virtual void Init(PlayerStateSystem playerSystem)
        {
            this.playerSystem = playerSystem;
            base.AddTransitions();
        }

        internal PlayerStateSystem StateSystem => playerSystem;

        internal Transform transform => playerSystem.transform;
        internal PlayerStateInput Input => playerSystem.InputValues;
        internal PlayerAnimation Animator => playerSystem.Animation;
        internal PlayerPhysics Physics => playerSystem.Physics;

        internal PlayerWeapons Weapons => playerSystem.Weapons;
        internal PlayerCamera Camera => playerSystem.Camera;
    }
}