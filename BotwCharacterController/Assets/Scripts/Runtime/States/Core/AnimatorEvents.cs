using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorEvents : MonoBehaviour
{
    public System.Action onAttackEnds;

    void AttackEnds()
    {
        Debug.Log("attack ends");
        onAttackEnds?.Invoke();
    }
}
