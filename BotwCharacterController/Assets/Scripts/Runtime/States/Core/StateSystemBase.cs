﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AGS.Player.FSM
{
    /// <summary>
    /// FSMSystem class represents the Finite State Machine class.
    ///  It has a List with the States the NPC has and methods to add,
    ///  delete a state, and to change the current state the Machine is on.
    /// </summary>
    public abstract class StateSystemBase : MonoBehaviour
    {
        [SerializeField] protected StateBase[] m_States;
        [SerializeField] protected StateBase m_CurrentState;

        public StateBase CurrentState => m_CurrentState;

        /// <summary>
        /// This method tries to change the state the FSM is in based on
        /// the current state and the transition passed.
        /// </summary>
        public bool PerformTransition( string transitionTo , params object[] _data )
        {
            // Check for NullTransition before changing the current state
            if ( string.IsNullOrEmpty( transitionTo ) )
            {
                Debug.LogError("PerformTransition >> NullTransition is not allowed");
                return false;
            }

            // Check if the currentState has the transition passed as argument
            var newState = m_CurrentState.GetOutputState( transitionTo );
            if ( newState == null)
            {
                Debug.LogError($"PerformTransition >> State {m_CurrentState} does not have state: '{transitionTo}'");
                return false;
            }

            if (false == newState.Active)
                return false;

            // Update the currentStateID and currentState		
            foreach ( var state in m_States )
            {
                if ( state == newState )
                {
                    // Do the post processing of the state before setting the new one
                    m_CurrentState.OnExit();

                    m_CurrentState = newState;

                    // Reset the state to its desired condition before it can reason or act
                    m_CurrentState.OnEnter( _data );
                    break;
                }
            }

            return true;
        } // PerformTransition()

    } //class FSMSystem
}
