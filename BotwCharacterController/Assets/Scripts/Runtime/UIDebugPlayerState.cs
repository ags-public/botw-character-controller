using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIDebugPlayerState : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI m_Label;
    string m_Log;

    bool m_Enabled = true;

    public void Enable(bool enable)
    {
        m_Enabled = 
        m_Label.enabled = enable;
    }

    public void AddLog(string addLog)
    {
        m_Log = string.Join("\n", addLog);
    }

    public void Show()
    {
        if(m_Enabled && false == string.IsNullOrEmpty(m_Log))
        {
            m_Label.text = m_Log;
        }
    }
}
